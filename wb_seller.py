import requests
import json
from datetime import datetime
import httplib2
import pandas as pd
import df2gspread as d2g
import configparser
from googleapiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials

today = datetime.now().date()
print(today)

config = configparser.ConfigParser()
config.read("/etc/bot/andrew/logistics/wb_seller/stocks", encoding="utf-8")
sheet = config['sheets']['guys']

def time(art, stock_name):
    url = "https://card.wb.ru/cards/detail?spp=0&regions=80,64,38,4,83,33,68,70,69,30,86,75,40,1,22,66,31,48,71&" \
              "pricemarginCoeff=1.0&reg=0&appType=1&emp=0&locale=ru&lang=ru&curr=rub&couponsGeo=" \
              "12,3,18,15,21&dest=-1257786&nm={art}".format(art=art)  # 5163995;6170054;31211424"

    res = requests.get(url, headers={
            "accept": "*/*",
            "accept-language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
            "sec-ch-ua": "\"Chromium\";v=\"110\", \"Not A(Brand\";v=\"24\", \"Google Chrome\";v=\"110\"",
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": "\"Windows\"",
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "cross-site"
        })

    res_json = json.loads(res.text)
    # print(res_json['data']['products'][0]['sizes'][0]['stocks'])
    for index in range(len(res_json['data']['products'][0]['sizes'][0]['stocks'])):
        # print(config['stocks'][str(res_json['data']['products'][0]['sizes'][0]['stocks'][index]['wh'])])
        if config['stocks'][str(res_json['data']['products'][0]['sizes'][0]['stocks'][index]['wh'])] == stock_name:
            print(res_json['data']['products'][0]['sizes'][0]['stocks'][index]['time2'])
            return res_json['data']['products'][0]['sizes'][0]['stocks'][index]['time2']
            break

scopes = ['https://www.googleapis.com/auth/spreadsheets']
creds_service = ServiceAccountCredentials.from_json_keyfile_name('/etc/bot/andrew/logistics/wb_seller/credits_guys.json', scopes).authorize(httplib2.Http())
table = build('sheets', 'v4', http=creds_service)
resp = table.spreadsheets().values().batchGet(spreadsheetId=sheet, ranges="Лист2").execute()

df_curr = pd.DataFrame(resp['valueRanges'][0]['values'])
df_curr.columns = df_curr.loc[0, :]
df_curr.drop(labels=0, axis=0, inplace=True)
print(df_curr)

cookies = {
    '_wbauid': '6966029861685089443',
    'BasketUID': '3c021030-ad85-45a8-a01b-14486dde4145',
    '___wbu': 'eeba0182-e3e4-4751-8791-06ddbaa799f0.1685089445',
    'WBToken': 'Atb4jgOWieTJDJbX7ecMQycZ28JHjCh1LmWYqLJrlBZWTKnktQUQ2PDmRHPMvyGZBLUWDSmDpghEGMzdUi5ki_IPeC6TwYpVsj1eOxVY4hcq17A',
    '___wbs': '6d026b53-8cdf-4a48-b5a6-524e726db586.1688627368',
    'WILDAUTHNEW_V3': '2ADA487C871B31CD83193AF1A7383F9226F427FD9E5D0CDFF482602B7EA6279411D8CBF0E64C796FA561AF08EC92CBA3FCC92578908E15E3EF73CDB717BF54189F9120290574CE34AAD87744B862F4D296CADACD363A0CAE058BE5B9B8323502607D5BFA537C375274CEEBD4D37A9463F8457BE635E97A9524882FBD6FC25E3F27D5EDEF3F048204B75A15226BAE03C4DC04D6EFAE5ECADA7A2AFC50FCEEF200B08E26F0AABA145487128D966D9064972A64BA6480F30D9706798E350ACC07721C23A5C1A45A01C652AC3F9262782DE305A6C9DD108211F447FFA8491100AFFE5D722D78AC28B8687C5E02161896CF0B53BAC057BE9C4A0E27945EDD3C409B5C0D1B26A75652B1B3048A56D2B9CD60483AAFD6E7A5A8A73E9398C4364D2C02FC417092FB',
    'um': 'uid%3Dw7TDssOkw7PCu8K3wrTCssK3wrPCtsK5%3Aproc%3D100%3Aehash%3Da09ea5a76fa90e5874ee15229adcfdf5',
}

headers = {
    'authority': 'seller-weekly-report.wildberries.ru',
    'accept': '*/*',
    'accept-language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
    'content-type': 'application/json',
    # 'cookie': '_wbauid=6966029861685089443; BasketUID=3c021030-ad85-45a8-a01b-14486dde4145; ___wbu=eeba0182-e3e4-4751-8791-06ddbaa799f0.1685089445; WBToken=Atb4jgOWieTJDJbX7ecMQycZ28JHjCh1LmWYqLJrlBZWTKnktQUQ2PDmRHPMvyGZBLUWDSmDpghEGMzdUi5ki_IPeC6TwYpVsj1eOxVY4hcq17A; ___wbs=6d026b53-8cdf-4a48-b5a6-524e726db586.1688627368; WILDAUTHNEW_V3=2ADA487C871B31CD83193AF1A7383F9226F427FD9E5D0CDFF482602B7EA6279411D8CBF0E64C796FA561AF08EC92CBA3FCC92578908E15E3EF73CDB717BF54189F9120290574CE34AAD87744B862F4D296CADACD363A0CAE058BE5B9B8323502607D5BFA537C375274CEEBD4D37A9463F8457BE635E97A9524882FBD6FC25E3F27D5EDEF3F048204B75A15226BAE03C4DC04D6EFAE5ECADA7A2AFC50FCEEF200B08E26F0AABA145487128D966D9064972A64BA6480F30D9706798E350ACC07721C23A5C1A45A01C652AC3F9262782DE305A6C9DD108211F447FFA8491100AFFE5D722D78AC28B8687C5E02161896CF0B53BAC057BE9C4A0E27945EDD3C409B5C0D1B26A75652B1B3048A56D2B9CD60483AAFD6E7A5A8A73E9398C4364D2C02FC417092FB; um=uid%3Dw7TDssOkw7PCu8K3wrTCssK3wrPCtsK5%3Aproc%3D100%3Aehash%3Da09ea5a76fa90e5874ee15229adcfdf5',
    'origin': 'https://seller.wildberries.ru',
    'referer': 'https://seller.wildberries.ru/',
    'sec-ch-ua': '"Not.A/Brand";v="8", "Chromium";v="114", "Google Chrome";v="114"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"Windows"',
    'sec-fetch-dest': 'empty',
    'sec-fetch-mode': 'cors',
    'sec-fetch-site': 'same-site',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36',
}

params = {
    'date': today,#'2023-07-06',
}

json_data = {
    'warehouse': 'asc',
}

response = requests.post(
    'https://seller-weekly-report.wildberries.ru/ns/categories-info/suppliers-portal-analytics/api/v1/tariffs-period',
    params=params,
    cookies=cookies,
    headers=headers,
    json=json_data,
)

df_main = pd.DataFrame({'Склад':[], 'Короба': [], 'Паллеты': []})

df_main = df_main.astype({'Склад':'str',
                        'Короба': 'int',
                        'Паллеты': 'int',
                        })

# print(response.json())

# print(df_main)
data = response.json()['data']['warehouseList']
for index in range(len(data)):
    if int(data[index]['palletVisibleExpr']) == 0 and int(data[index]['boxDeliveryAndStorageVisibleExpr']) == 0:
        ok = 1
    else:
    # print(data[index]['warehouseName'], data[index]['boxDeliveryAndStorageExpr'],
    #       data[index]['palletDeliveryExpr'], data[index]['palletStorageExpr'])
        df_main.loc[len(df_main.index)] = [data[index]['warehouseName'],
                                       int(data[index]['boxDeliveryAndStorageExpr']) * 0.5 * int(data[index]['boxDeliveryAndStorageVisibleExpr']),
                                       int(data[index]['palletDeliveryExpr']) * 0.5 * int(data[index]['palletVisibleExpr'])]

print(df_main['Склад'].tolist())
time_list = []
for stock in df_main['Склад'].tolist():
    # df = df_curr.loc[(df_curr['Склад'] == df_main['Склад'].tolist()[7]) & (df_curr['Остаток'] != str(0))]
    df = df_curr.loc[(df_curr['Склад'] == stock) & (df_curr['Остаток'] != str(0))]

    try:
        art = df['Артикул'].tolist()[0]
        print(df['Артикул'].tolist()[0], stock)
        delivery_time = time(art=art, stock_name=stock)
    except:
        # print('no articule')
        delivery_time = 0

    print(delivery_time)
    time_list.append(delivery_time)

df_main['Сроки Москва'] = time_list
df_main = df_main.fillna(0)
print(df_main)

body = {
        'data': [
            {
                'range': 'Логистика',
                'majorDimension': 'COLUMNS',
                'values': [df_main.columns.values.tolist()]
            },
            {
                'range' : 'Логистика',
                'majorDimension': 'ROWS',
                'values': df_main.values.tolist()
            }
        ]
    }

request = table.spreadsheets().values().update(spreadsheetId=sheet,
                                                  valueInputOption='USER_ENTERED',
                                                  range='Логистика',
                                                  body={'values': [df_main.columns.tolist()] + df_main.values.tolist()}).execute()